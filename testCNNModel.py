# Convolutional network building
from __future__ import division, print_function, absolute_import

import tflearn
from tflearn.data_utils import shuffle, to_categorical
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
import os
import numpy as np
import cv2
import argparse

ROOT_DIR = 'E:/Austen/Documents/PycharmProjects/TensorFlow/nn_train/'

network = input_data(shape=[None, 32, 32, 3])

# modified from https://medium.com/@husseinjaafar/my-first-cnn-in-tflearn-1019f88485f3
network = conv_2d(network, 32, 5, padding='same', activation='relu')
network = max_pool_2d(network, kernel_size=2)
network = conv_2d(network, 64, 5, padding='same', activation='relu')
network = max_pool_2d(network, kernel_size=2)
network = fully_connected(network, 1024, activation='relu')
network = dropout(network, 0.6)
network = fully_connected(network, 2, activation='softmax')
network = regression(network, optimizer='adam', learning_rate=0.001, loss='categorical_crossentropy', name='targets')

# Train using classifier
model = tflearn.DNN(network, tensorboard_verbose=0)

########### Lone_Test ###################################

if __name__ == "__main__":
    model.load('model.tfl')
    test_path = ROOT_DIR + 'lone_test'
    test_data = {}
    test = 0

    for root, dirs, files in os.walk(test_path):
        for item in files:
            if item.endswith('.png'):
                x = os.path.join(root, item)
                img = (cv2.imread(x) / float(255))
                test_data[test] = img
                test += 1

    X_test_data = np.array(list(test_data.values()))
    predictions = model.predict(X_test_data)

    for en in enumerate(predictions):
        print(en)