# -*- coding: utf-8 -*-

## VK: I need to run this after I run resize_image.py

from __future__ import division, print_function, absolute_import

import tflearn
from tflearn.data_utils import shuffle, to_categorical
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
import os
import numpy as np
import cv2
import argparse

ROOT_DIR = 'E:/Austen/Documents/PycharmProjects/TensorFlow/nn_train/'
ap = argparse.ArgumentParser()
ap.add_argument('-snnp', '--snnp', required=True,
                help='Path where trained conv nn will be saved')
ap.add_argument('-n_epoch', '--n_epoch', required=True,
                help='how many epocs to train conv nn for')
args = vars(ap.parse_args())

# Data loading and preprocessing

data = {}
data_test = {}

### These are target arrays. Y is
### the target array for data and Y_test
### is the target array for data_test.
Y = []
Y_test = []

### i is the global_counter for bee
### j is the global counter for no-bee
i = 0
j = 0
###########Buzz Train#################
## THIS IS A YES_BEE_TRAIN DIRECTORY
YES_BEE_TRAIN = ROOT_DIR + 'single_bee_train'

for root, dirs, files in os.walk(YES_BEE_TRAIN):
    for item in files:
        if item.endswith('.png'):
            x = os.path.join(root, item)
            img = (cv2.imread(x) / float(255))
            data[i] = img
            Y.append(int(1))
            i += 1  # added indent - if !png file then don't count it

######################################

########### Buzz Tes t#################
# YES_BEE_TEST DIRECTORY
YES_BEE_TEST = ROOT_DIR + 'single_bee_test'

for root, dirs, files in os.walk(YES_BEE_TEST):
    for item in files:
        if item.endswith('.png'):
            x = os.path.join(root, item)
            img = (cv2.imread(x) / float(255))
            # print img.shape
            data_test[j] = img
            Y_test.append(int(1))
            j += 1

######################################

###########Noise Train#################
# NO_BEE_TRAIN DIRECTORY
NO_BEE_TRAIN = ROOT_DIR + 'no_bee_train'

for root, dirs, files in os.walk(NO_BEE_TRAIN):
    for item in files:
        if item.endswith('.png'):
            x = os.path.join(root, item)
            img = (cv2.imread(x) / float(255))
            data[i] = img
            Y.append(int(0))
            i += 1

######################################

########### Noise Test #################
# NO_BEE_TEST DIRECTORY
NO_BEE_TEST = ROOT_DIR + 'no_bee_test'

for root, dirs, files in os.walk(NO_BEE_TEST):
    for item in files:
        if item.endswith('.png'):
            x = os.path.join(root, item)
            img = (cv2.imread(x) / float(255))
            data_test[j] = img
            Y_test.append(int(0))
            j += 1

######################################

########### Data Preprocessing ################
# Python 3 returns a view that must be converted to a list
X = np.array(list(data.values()))
X_test = np.array(list(data_test.values()))

X, Y = shuffle(X, Y)
##############################################

# print (X.shape)
Y = to_categorical(Y, 2)
Y_test = to_categorical(Y_test, 2)

# Real-time data preprocessing
img_prep = ImagePreprocessing()
img_prep.add_featurewise_zero_center()
img_prep.add_featurewise_stdnorm()

# Real-time data augmentation
img_aug = ImageAugmentation()
img_aug.add_random_flip_leftright()
img_aug.add_random_rotation(max_angle=25.)

# Convolutional network building
network = input_data(shape=[None, 32, 32, 3],
                     data_preprocessing=img_prep,
                     data_augmentation=img_aug)

# modified from https://medium.com/@husseinjaafar/my-first-cnn-in-tflearn-1019f88485f3
network = conv_2d(network, 32, 5, padding='same', activation='relu')
network = max_pool_2d(network, kernel_size=2)
network = conv_2d(network, 64, 5, padding='same', activation='relu')
network = max_pool_2d(network, kernel_size=2)
network = fully_connected(network, 1024, activation='relu')
network = dropout(network, 0.6)
network = fully_connected(network, 2, activation='softmax')
network = regression(network, optimizer='adam', learning_rate=0.001, loss='categorical_crossentropy', name='targets')

# Train using classifier
model = tflearn.DNN(network, tensorboard_verbose=0)

n_epoch = int(args['n_epoch'])
model.fit(X, Y, n_epoch=n_epoch, shuffle=True, validation_set=(X_test, Y_test),
          show_metric=True, batch_size=512, run_id='SINGLE_BEE_CNN')

model.save('model100.tfl')

